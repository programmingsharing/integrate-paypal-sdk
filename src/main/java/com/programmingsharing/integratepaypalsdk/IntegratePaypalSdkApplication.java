package com.programmingsharing.integratepaypalsdk;

import com.programmingsharing.integratepaypalsdk.services.PaymentService;
import com.programmingsharing.integratepaypalsdk.services.PaymentServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class IntegratePaypalSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegratePaypalSdkApplication.class, args);
    }

    @Bean
    public PaymentService paymentService(){
        return new PaymentServiceImpl();
    }

}
