package com.programmingsharing.integratepaypalsdk.fixture;

import com.programmingsharing.integratepaypalsdk.domain.Product;

import java.util.ArrayList;

public class ProductListFixture {
    private static final ArrayList<Product> productList;

    static {
        productList = new ArrayList<>();
        productList.add(new Product(1, "Product 1", 12));
        productList.add(new Product(2, "Product 2", 13));
        productList.add(new Product(3, "Product 3", 14));
        productList.add(new Product(4, "Product 4", 15));
        productList.add(new Product(5, "Product 5", 16));
    }

    public static ArrayList<Product> getProductList() {
        return productList;
    }
}
