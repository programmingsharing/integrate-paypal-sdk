package com.programmingsharing.integratepaypalsdk.services;

import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.programmingsharing.integratepaypalsdk.model.PaypalPaymentModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private APIContext apiContext;

    public Payment createPayment(PaypalPaymentModel paymentModel) throws PayPalRESTException {
        Double total = paymentModel.getTotal();

        ItemList itemList = new ItemList();
        itemList.setItems(paymentModel.getItems());

        Amount amount = new Amount();
        amount.setCurrency(paymentModel.getCurrency());
        total = new BigDecimal(total).setScale(2, RoundingMode.HALF_UP).doubleValue();
        amount.setTotal(String.format("%.2f", total));

        Transaction transaction = new Transaction();
        transaction.setDescription(paymentModel.getDescription());
        transaction.setAmount(amount);
        transaction.setItemList(itemList);

        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        Payer payer = new Payer();
        payer.setPaymentMethod(paymentModel.getPaymentMethod());

        Payment payment = new Payment();
        payment.setIntent(paymentModel.getPaymentIntent());
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(paymentModel.getCancelUrl());
        redirectUrls.setReturnUrl(paymentModel.getSuccessUrl());
        payment.setRedirectUrls(redirectUrls);

        return payment.create(apiContext);
    }

    public Payment executePayment(String paymentId, String payerId) throws PayPalRESTException {
        Payment payment = new Payment();
        payment.setId(paymentId);
        PaymentExecution paymentExecute = new PaymentExecution();
        paymentExecute.setPayerId(payerId);
        return payment.execute(apiContext, paymentExecute);
    }
}
