package com.programmingsharing.integratepaypalsdk.services;

import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.programmingsharing.integratepaypalsdk.model.PaypalPaymentModel;

public interface PaymentService {

    Payment createPayment(PaypalPaymentModel paymentModel) throws PayPalRESTException;

    Payment executePayment(String paymentId, String payerId) throws PayPalRESTException;
}
