package com.programmingsharing.integratepaypalsdk.services;

import com.paypal.base.rest.PayPalRESTException;
import com.programmingsharing.integratepaypalsdk.domain.Product;

import java.util.ArrayList;

public interface OrderService {
    String processOrder(ArrayList<Product> productList) throws PayPalRESTException;
}
