package com.programmingsharing.integratepaypalsdk.services;

import com.paypal.api.payments.Item;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.programmingsharing.integratepaypalsdk.domain.Product;
import com.programmingsharing.integratepaypalsdk.fixture.ProductListFixture;
import com.programmingsharing.integratepaypalsdk.model.PaypalPaymentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private PaymentService paymentService;

    @Override
    public String processOrder(ArrayList<Product> productList) throws PayPalRESTException {
        List<Item> items = new ArrayList<>();
        AtomicReference<Double> totalAmount = new AtomicReference<>((double) 0);
        ProductListFixture.getProductList().forEach(product -> {
            Item item = new Item();
            item.setCurrency("USD");
            item.setName(product.getName());
            item.setPrice(String.format("%.2f", new BigDecimal(product.getPrice()).setScale(2, RoundingMode.HALF_UP)));
            item.setQuantity("1");
            item.setSku(String.valueOf(product.getId()));
            items.add(item);
            totalAmount.set(totalAmount.get() + product.getPrice());
        });

        PaypalPaymentModel paymentModel = new PaypalPaymentModel();
        paymentModel.setCancelUrl("http://locahost:8080/payment-failed");
        paymentModel.setCurrency("USD");
        paymentModel.setSuccessUrl("http://localhost:8080/payment-success");
        paymentModel.setDescription("Integrate Paypal Payment");
        paymentModel.setPaymentIntent("ORDER");// Must be set Supported values are SALE, AUTHORIZE, ORDER, NONE
        paymentModel.setTotal(totalAmount.get());
        paymentModel.setItems(items);
        paymentModel.setPaymentMethod("PAYPAL"); // Must be set, Supported values are CREDIT_CARD, PAYPAL, BANK, CARRIER, ALTERNATE_PAYMENT, PAY_UPON_INVOICE

        try {
            Payment payment = paymentService.createPayment(paymentModel);
            for (Links links : payment.getLinks()) {
                if (links.getRel().equals("approval_url")) {
                    return links.getHref();
                }
            }
        } catch (PayPalRESTException e) {
            e.printStackTrace();
        }
        return "/";
    }
}
