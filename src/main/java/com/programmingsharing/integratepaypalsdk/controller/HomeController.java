package com.programmingsharing.integratepaypalsdk.controller;

import com.programmingsharing.integratepaypalsdk.fixture.ProductListFixture;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String index(ModelMap modelMap){
        modelMap.addAttribute("productList", ProductListFixture.getProductList());

        return "product-list";
    }


}
