package com.programmingsharing.integratepaypalsdk.controller;

import com.paypal.base.rest.PayPalRESTException;
import com.programmingsharing.integratepaypalsdk.fixture.ProductListFixture;
import com.programmingsharing.integratepaypalsdk.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PaymentController {

    @Autowired
    private OrderService orderService;

    @GetMapping("process-payment")
    public String processPayment() throws PayPalRESTException {
        String processUrl="";
        try {
            processUrl = orderService.processOrder(ProductListFixture.getProductList());
        } catch (PayPalRESTException e) {
            return "redirect:/payment-failed";
        }
        return "redirect:"+processUrl;
    }

    @GetMapping("payment-failed")
    public String returnFailed() {
        return "payment/failed";
    }

    @GetMapping("payment-success")
    public String returnSuccess() {
        return "payment/success";
    }
}
